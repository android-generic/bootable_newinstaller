# Copyright 2009-2014, The Android-x86 Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
BUILD_TOP := $(shell pwd)

ifneq ($(filter x86%,$(TARGET_ARCH)),)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
RELEASE_OS_TITLE := AG-10
include $(CLEAR_VARS)
LOCAL_IS_HOST_MODULE := true
LOCAL_SRC_FILES := rpm/qemu-android
LOCAL_MODULE := $(notdir $(LOCAL_SRC_FILES))
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_POST_INSTALL_CMD := $(hide) sed -i "s|CMDLINE|$(BOARD_KERNEL_CMDLINE)|" $(HOST_OUT_EXECUTABLES)/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

VER ?= $$(date +"%F")
VERS ?= $$(date +"%Y%m%d")
#~ VERS ?= $$(date +"%Y%m%d%H%M")

# use squashfs for iso, unless explictly disabled
ifneq ($(USE_SQUASHFS),0)
MKSQUASHFS := $(MAKE_SQUASHFS)

define build-squashfs-target
	$(hide) $(MKSQUASHFS) $(1) $(2) -noappend -comp gzip
endef
endif

# Bliss Build Colors 
include $(LOCAL_PATH)/bliss/bliss_colors.mk 

initrd_dir := $(LOCAL_PATH)/initrd
initrd_bin := \
	$(initrd_dir)/init \
	$(wildcard $(initrd_dir)/*/*)

systemimg  := $(PRODUCT_OUT)/system.$(if $(MKSQUASHFS),sfs,img)

TARGET_INITRD_OUT := $(PRODUCT_OUT)/initrd
INITRD_RAMDISK := $(TARGET_INITRD_OUT).img
$(INITRD_RAMDISK): $(initrd_bin) $(systemimg) $(TARGET_INITRD_SCRIPTS) | $(ACP) $(MKBOOTFS)
	$(hide) rm -rf $(TARGET_INITRD_OUT)
	mkdir -p $(addprefix $(TARGET_INITRD_OUT)/,android apex hd iso lib mnt proc scripts sfs sys tmp)
	$(if $(TARGET_INITRD_SCRIPTS),$(ACP) -p $(TARGET_INITRD_SCRIPTS) $(TARGET_INITRD_OUT)/scripts)
	ln -s /bin/ld-linux.so.2 $(TARGET_INITRD_OUT)/lib
	echo "VER=$(VER)" > $(TARGET_INITRD_OUT)/scripts/00-ver
	$(if $(RELEASE_OS_TITLE),echo "OS_TITLE=$(RELEASE_OS_TITLE)" >> $(TARGET_INITRD_OUT)/scripts/00-ver)
	$(if $(INSTALL_PREFIX),echo "INSTALL_PREFIX=$(INSTALL_PREFIX)" >> $(TARGET_INITRD_OUT)/scripts/00-ver)
	$(MKBOOTFS) $(<D) $(TARGET_INITRD_OUT) | gzip -9 > $@

INSTALL_RAMDISK := $(PRODUCT_OUT)/install.img
INSTALLER_BIN := $(TARGET_INSTALLER_OUT)/sbin/efibootmgr
$(INSTALL_RAMDISK): $(wildcard $(LOCAL_PATH)/install/*/* $(LOCAL_PATH)/install/*/*/*/*) $(INSTALLER_BIN) | $(MKBOOTFS)
	$(if $(TARGET_INSTALL_SCRIPTS),mkdir -p $(TARGET_INSTALLER_OUT)/scripts; $(ACP) -p $(TARGET_INSTALL_SCRIPTS) $(TARGET_INSTALLER_OUT)/scripts)
	$(MKBOOTFS) $(dir $(dir $(<D))) $(TARGET_INSTALLER_OUT) | gzip -9 > $@

isolinux_files := $(addprefix external/syslinux/bios/com32/, \
	../core/isolinux.bin \
	chain/chain.c32 \
	elflink/ldlinux/ldlinux.c32 \
	lib/libcom32.c32 \
	libutil/libutil.c32 \
	menu/vesamenu.c32)

boot_dir := $(PRODUCT_OUT)/boot
$(boot_dir): $(shell find $(LOCAL_PATH)/boot -type f | sort -r) $(isolinux_files) $(systemimg) $(INSTALL_RAMDISK) $(GENERIC_X86_CONFIG_MK) | $(ACP)
	$(hide) rm -rf $@
	$(ACP) -pr $(dir $(<D)) $@
	$(ACP) -pr $(dir $(<D))../install/grub2/efi $@
	$(ACP) $(isolinux_files) $@/isolinux
	PATH="/sbin:/usr/sbin:/bin:/usr/bin"; \
	img=$@/boot/grub/efi.img; dd if=/dev/zero of=$$img bs=1M count=4; \
	mkdosfs -n EFI $$img; mmd -i $$img ::boot; \
	mcopy -si $$img $@/efi ::; mdel -i $$img ::efi/boot/*.cfg

BUILT_IMG := $(addprefix $(PRODUCT_OUT)/,initrd.img install.img Android-x86-Installer.exe gearlock) $(systemimg)
BUILT_IMG += $(if $(TARGET_PREBUILT_KERNEL),$(TARGET_PREBUILT_KERNEL),$(PRODUCT_OUT)/kernel)

# Grab branch names
KRNL := $(shell cd $(BUILD_TOP)/kernel ; git name-rev --name-only HEAD | cut -d '/' -f3)
MSA := $(shell cd $(BUILD_TOP)/external/mesa ; git name-rev --name-only HEAD | cut -d '/' -f3)
DG := $(shell cd $(BUILD_TOP)/external/drm_gralloc ; git name-rev --name-only HEAD | cut -d '/' -f3)
DHW := $(shell cd $(BUILD_TOP)/external/drm_hwcomposer ; git name-rev --name-only HEAD | cut -d '/' -f3)
LD := $(shell cd $(BUILD_TOP)/external/libdrm ; git name-rev --name-only HEAD | cut -d '/' -f3)
MG := $(shell cd $(BUILD_TOP)/external/minigbm ; git name-rev --name-only HEAD | cut -d '/' -f3)
FW := $(shell cd $(BUILD_TOP)/device/generic/firmware ; git name-rev --name-only HEAD | cut -d '/' -f3)
DGC := $(shell cd $(BUILD_TOP)/device/generic/common ; git name-rev --name-only HEAD | cut -d '/' -f3)
# Grab enabled extras
ifeq ($(USE_GMS),true)
GMS := "_gms"
else
GMS := ""
endif

ifeq ($(USE_FDROID),true)
FDR := "_fdroid"
else
FDR := ""
endif

ifeq ($(USE_FOSS),true)
FOS := "_foss"
else
FOS := ""
endif

ifeq ($(USE_HOUDINI),true)
HOU := "_cros-hd"
else
HOU := ""
endif

ifeq ($(USE_WIDEVINE),true)
WDV := "_cros-wv"
else
WDV := ""
endif

# Use vendor defined version names
ROM_VENDOR_VERSION := $(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)

ifneq ($(wildcard $(BUILD_TOP)/vendor/bliss/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "Bliss-OS-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/lineage/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "LineageOS-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/tesla/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "Tesla-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/tipsy/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "Tipsy-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/carbon/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "CarbonROM-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/aosp/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "PixelExperience-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/du/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "DirtyUnicorns-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/bootleggers/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "Bootleggers-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

ifneq ($(wildcard $(BUILD_TOP)/vendor/omni/.*),)
ifneq ($(ROM_VENDOR_CUSTOM_NAME),"")
ROM_VENDOR_VERSION := "OmniROM-$(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)"
else
ROM_VENDOR_VERSION := $(ROM_VENDOR_CUSTOM_NAME)
endif
endif

BUILD_NAME_VARIANT := $(RELEASE_OS_TITLE)-$(ROM_VENDOR_VERSION)

ISO_IMAGE := $(PRODUCT_OUT)/$(BUILD_NAME_VARIANT)_k-$(KRNL)_m-$(MSA)_dgc-$(DGC)$(GMS)$(FDR)$(FOS)$(HOU)$(WDV).iso
$(ISO_IMAGE): $(boot_dir) $(BUILT_IMG)
	# Generate Changelog
	$(hide) ./vendor/android-generic/tools/changelog
	$(hide) mv $(PRODUCT_OUT)/Changelog.txt $(PRODUCT_OUT)/Changelog-$(BUILD_NAME_VARIANT)-$(shell date +%Y%m%d%H%M).txt
	@echo ----- Making iso image ------
	$(hide) sed -i "s|\(Installation CD\)\(.*\)|\1 $(VER)|; s|CMDLINE|$(BOARD_KERNEL_CMDLINE)|" $</isolinux/isolinux.cfg
	$(hide) sed -i "s|VER|$(VER)|; s|CMDLINE|$(BOARD_KERNEL_CMDLINE)|" $</efi/boot/android.cfg
	sed -i "s|OS_TITLE|$(if $(RELEASE_OS_TITLE),$(RELEASE_OS_TITLE),Android-x86)|" $</isolinux/isolinux.cfg $</efi/boot/android.cfg
	PATH="/sbin:/usr/sbin:/bin:/usr/bin"; \
	which xorriso > /dev/null 2>&1 && GENISOIMG="xorriso -as mkisofs" || GENISOIMG=genisoimage; \
	$$GENISOIMG -vJURT -b isolinux/isolinux.bin -c isolinux/boot.cat \
		-no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot \
		-input-charset utf-8 -V "$(if $(RELEASE_OS_TITLE),$(RELEASE_OS_TITLE),Android-x86) $(VER) ($(TARGET_ARCH))" -o $@ $^
	$(hide) external/syslinux/bios/utils/isohybrid.pl $@
	
	@echo -e ${CL_CYN}""${CL_CYN} 
	@echo -e ${CL_CYN}"      ___           ___                   ___           ___      "${CL_CYN} 
	@echo -e ${CL_CYN}"     /\  \         /\__\      ___        /\  \         /\  \     "${CL_CYN} 
	@echo -e ${CL_CYN}"    /::\  \       /:/  /     /\  \      /::\  \       /::\  \    "${CL_CYN} 
	@echo -e ${CL_CYN}"   /:/\:\  \     /:/  /      \:\  \    /:/\ \  \     /:/\ \  \   "${CL_CYN} 
	@echo -e ${CL_CYN}"  /::\~\:\__\   /:/  /       /::\__\  _\:\~\ \  \   _\:\~\ \  \  "${CL_CYN} 
	@echo -e ${CL_CYN}" /:/\:\ \:\__\ /:/__/     __/:/\/__/ /\ \:\ \ \__\ /\ \:\ \ \__\ "${CL_CYN} 
	@echo -e ${CL_CYN}" \:\~\:\/:/  / \:\  \    /\/:/  /    \:\ \:\ \/__/ \:\ \:\ \/__/ "${CL_CYN} 
	@echo -e ${CL_CYN}"  \:\ \::/  /   \:\  \   \::/__/      \:\ \:\__\    \:\ \:\__\   "${CL_CYN} 
	@echo -e ${CL_CYN}"   \:\/:/  /     \:\  \   \:\__\       \:\/:/  /     \:\/:/  /   "${CL_CYN} 
	@echo -e ${CL_CYN}"    \::/__/       \:\__\   \/__/        \::/  /       \::/  /    "${CL_CYN} 
	@echo -e ${CL_CYN}"     ~~            \/__/                 \/__/         \/__/     "${CL_CYN} 
	@echo -e ${CL_CYN}""${CL_CYN} 
	@echo -e ${CL_CYN}"===========-Bliss-x86 Package Complete-==========="${CL_RST} 
	@echo -e ${CL_CYN}"Zip: "${CL_CYN} $(ISO_IMAGE)${CL_RST} 
	@echo -e ${CL_CYN}"Size:"${CL_CYN}" `ls -lah $(ISO_IMAGE) | cut -d ' ' -f 5`"${CL_RST} 
	@echo -e ${CL_CYN}"=================================================="${CL_RST} 
	@echo -e ${CL_CYN}"        Have A Truly Blissful Experience"          ${CL_RST} 
	@echo -e ${CL_CYN}"=================================================="${CL_RST} 
	@echo -e "" 

	@echo -e "\n\n$@ is built successfully.\n\n"

rpm: $(wildcard $(LOCAL_PATH)/rpm/*) $(BUILT_IMG)
	@echo ----- Making an rpm ------
	OUT=$(abspath $(PRODUCT_OUT)); mkdir -p $$OUT/rpm/BUILD; rm -rf $$OUT/rpm/RPMS/*; $(ACP) $< $$OUT; \
	echo $(VERS) | grep -vq rc; EPOCH=$$((-$$? + `echo $(VERS) | cut -d. -f1`)); \
	PATH="/sbin:/usr/sbin:/bin:/usr/bin"; \
	rpmbuild -bb --target=$(if $(filter x86,$(TARGET_ARCH)),i686,x86_64) -D"cmdline $(BOARD_KERNEL_CMDLINE)" \
		-D"_topdir $$OUT/rpm" -D"_sourcedir $$OUT" -D"systemimg $(notdir $(systemimg))" -D"ver $(VERS)" -D"epoch $$EPOCH" \
		$(if $(BUILD_NAME_VARIANT),-D"name $(BUILD_NAME_VARIANT)") \
		-D"install_prefix $(if $(INSTALL_PREFIX),$(INSTALL_PREFIX),android-$(VERS))" $(filter %.spec,$^); \
	mv $$OUT/rpm/RPMS/*/*.rpm $$OUT

.PHONY: iso_img usb_img efi_img rpm
iso_img: $(ISO_IMAGE)
usb_img: $(ISO_IMAGE)
efi_img: $(ISO_IMAGE)

endif
